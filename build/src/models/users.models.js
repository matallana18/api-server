"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
/** Model Users */
const mongoose_1 = require("mongoose");
const aux_users_aux_1 = require("../controllers/aux_methods/aux_users.aux");
const errors_1 = __importDefault(require("../lib/errors"));
const user_schema = new mongoose_1.Schema({
    name: { type: String, minlength: [3, 'ho is u name??... (min 3 characters)'] },
    last_name: { type: String, minlength: [3, 'ho is u last name??... (min 3 characters)'] },
    alias: { type: String, unique: [true, 'this alias is asignate of the other member.. try again pls!'], minlength: [2, 'it could be something like lazy bob (min 2 characters)'] },
    user: { type: String, required: [true, 'tell us your email pls.. friend!'] },
    password: { type: String, required: [true, 'just something u should know...'], minlength: [32, 'error.. password hash invalid'], select: false }
});
user_schema.pre('validate', function (next) {
    try {
        const password = this.get('password');
        const new_password = aux_users_aux_1.transform_key(password);
        if (typeof new_password === 'boolean') {
            throw new errors_1.default('Error in create user.', 'Errors validations', 403, 'Register Controllers', [{ message: 'Min length of characters is 8.. pls try again..', key: 'password' }]);
        }
        else {
            this.set('password', new_password);
        }
        next();
    }
    catch (error) {
        next(error);
    }
});
exports.default = mongoose_1.model('User', user_schema);
