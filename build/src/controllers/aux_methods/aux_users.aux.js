"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const md5_1 = __importDefault(require("md5"));
const errors_1 = __importDefault(require("../../lib/errors"));
const handleErrorsMongoDB_1 = require("../../lib/handleErrorsMongoDB");
const transform_key = (password) => {
    if (password.length >= 8) {
        return md5_1.default(password);
    }
    else {
        return false;
    }
};
exports.transform_key = transform_key;
const orquestrator_errors = (e, next) => {
    if (typeof e.code !== 'undefined' && e.code === 11000) {
        const mensaje = [];
        mensaje.push({
            key: 'Alias',
            message: 'this alias is asignate of the other member.. try again pls!'
        });
        next(new errors_1.default('Error in create user.', 'Errors validations', 403, 'Register Controllers', mensaje));
    }
    if (e.errors) {
        const mensaje = handleErrorsMongoDB_1.HandleErrorsMongose(e.errors);
        next(new errors_1.default('Error in create user.', 'Errors validations', 403, 'Register Controllers', mensaje));
    }
    next(e);
};
exports.orquestrator_errors = orquestrator_errors;
