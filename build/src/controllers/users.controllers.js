"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const users_models_1 = __importDefault(require("../models/users.models"));
const aux_users_aux_1 = require("./aux_methods/aux_users.aux");
const errors_1 = __importDefault(require("../lib/errors"));
class UserController {
    constructor() {
        this.get_all_users = (req, res, next) => __awaiter(this, void 0, void 0, function* () {
            try {
                const users = yield users_models_1.default.find().lean().exec();
                if (!users) {
                    throw new errors_1.default('Error in search users', 'Errors searchs', 403, 'Search Controllers', [{ key: 'search', message: 'not found any user' }]);
                }
                res.status(200).json({
                    name: `Get all users`,
                    message: `TEAM`,
                    data: users,
                    from: `Users controlers`
                });
            }
            catch (error) {
                next(error);
            }
        });
        this.register_users = (req, res, next) => __awaiter(this, void 0, void 0, function* () {
            try {
                const user = req.body;
                const save_user = yield users_models_1.default.create(user);
                res.status(200).json({
                    name: `User created by ${save_user.alias || save_user.name}`,
                    message: `Congrats ${save_user.alias || save_user.name}, you are ready in this site!`,
                    data: save_user,
                    from: `Users controlers`
                });
            }
            catch (e) {
                /** generar logica de mensajes, para n errores y un error */
                aux_users_aux_1.orquestrator_errors(e, next);
            }
        });
    }
}
exports.UserController = UserController;
