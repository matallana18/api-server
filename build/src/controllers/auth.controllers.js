"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const users_models_1 = __importDefault(require("../models/users.models"));
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const errors_1 = __importDefault(require("../lib/errors"));
const md5_1 = __importDefault(require("md5"));
const key_1 = require("../server/keys/key");
/* const app = express(); */
class AuthController {
    constructor() {
        this.login = (req, res, next) => __awaiter(this, void 0, void 0, function* () {
            try {
                const credentials = req.body;
                console.log({ credentials });
                const user_email_valid = yield users_models_1.default.findOne({ user: credentials.user }).lean().exec();
                if (!user_email_valid) {
                    throw new errors_1.default('Auth Error', 'User not found', 404, 'Auth Controllers', [{ key: 'user or password', message: 'user or password wrong... try again pls' }]);
                }
                const user_password_valid = yield users_models_1.default.findOne({ password: md5_1.default(credentials.password) }).lean().exec();
                if (!user_password_valid) {
                    throw new errors_1.default('Auth Error', 'User not found', 404, 'Auth Controllers', [{ key: 'user or password', message: 'user or password wrong... try again pls' }]);
                }
                const token = jsonwebtoken_1.default.sign(user_password_valid, key_1.SECRET_KEY, { expiresIn: '2h' });
                const login = { expirte_in: '2h', token, user: user_password_valid };
                res.json(login);
            }
            catch (error) {
                next(error);
            }
        });
    }
}
exports.AuthController = AuthController;
