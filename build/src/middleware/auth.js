"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const key_1 = require("../server/keys/key");
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
function auth(request, response, next) {
    let token = request.get('token');
    if (token !== null) {
        jsonwebtoken_1.default.verify(token, key_1.SECRET_KEY, (err, decoded) => {
            if (err) {
                switch (err.name) {
                    case 'TokenExpiredError':
                        response.status(401).json({
                            name: 'Token expired',
                            message: 'Token expired in server',
                            from: 'Validation token'
                        });
                        break;
                    case 'JsonWebTokenError':
                        response.status(401).json({
                            name: 'Token wrong',
                            message: 'Token wrong in server',
                            from: 'Validation token'
                        });
                        break;
                    default:
                        response.status(401).json({
                            name: 'Token wrong',
                            message: 'Token wrong in server',
                            from: 'Validation token'
                        });
                        break;
                }
            }
            else {
                next();
            }
        });
    }
    else {
        response.status(401).json({
            name: 'Token wrong',
            message: 'Token wrong in server',
            from: 'Validation token'
        });
    }
}
exports.default = auth;
