"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function ExceptionError(error, req, res, next) {
    res.status(error.status || 500).json({
        name: error.name,
        message: error.message,
        group_messages: (error.messages) ? error.messages : [],
        from: error.from
    });
    next();
}
exports.default = ExceptionError;
