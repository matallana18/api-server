"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const users_controllers_1 = require("../controllers/users.controllers");
const router = express_1.Router();
const co_us = new users_controllers_1.UserController();
router.get('/all_users', co_us.get_all_users);
router.post('/new', co_us.register_users);
exports.default = router;
