"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const auth_controllers_1 = require("../controllers/auth.controllers");
const router = express_1.Router();
const co_auth = new auth_controllers_1.AuthController();
router.post('/login', co_auth.login);
exports.default = router;
