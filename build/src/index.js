"use strict";
/**
 * DESCRIPCIÓN DIRECTORIO SRC:
 * src /
 *      Controllers/:Todos los archivos que se encargaran de orquestar la aplicación
 *      lib/: Todos los archivos con codigo compartido entre la aplicación
 *      models/: Todos los archivos de modelos de la aplicación
 *      routes/: Todos los archivos de rutas de la aplicación
 *      views/: Todas las vistas de la aplicacíon
 *      server/: Archivos de servidor.
 *      index.ts : Punto de entrada de la aplicación
 *
 * tips:
 *      npm install @types/express ayuda al tipado de typescripte para el framework express
 *
 * Comandos configurados:
 *      npm run clean: (rm || del in windows -rf build) comando para eliminar la carpeta build que genera tsc 'typescriptcompiler'
 */
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const morgan_1 = __importDefault(require("morgan"));
const cors_1 = __importDefault(require("cors"));
const env_config_1 = require("./environment/env_config");
const index_routes_routes_1 = __importDefault(require("./routes/index_routes.routes"));
const server_1 = __importDefault(require("./server/server"));
const handleErrors_1 = __importDefault(require("./middleware/handleErrors"));
/* import routes from './routes/' */
const server = server_1.default.init_server(env_config_1.PORT);
/** Settings server*/
server.app.use(morgan_1.default('dev'));
server.app.use(express_1.default.json());
/** entender datos de formulario */
server.app.use(express_1.default.urlencoded({ extended: false }));
server.app.use(cors_1.default({ origin: true }));
/** Middlewares */
/** Routes */
server.app.use('/api/inv-v1/', index_routes_routes_1.default);
/** Static files */
server.app.use(handleErrors_1.default);
/** Start server */
server.start_server(() => {
    console.log('initbd');
    console.log('Server started on ports:', env_config_1.PORT);
});
