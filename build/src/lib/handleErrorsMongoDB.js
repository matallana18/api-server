"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function HandleErrorsMongose(errores) {
    const mensaje = [];
    for (let key in errores) {
        mensaje.push({ message: errores[key].message, key });
    }
    return mensaje;
}
exports.HandleErrorsMongose = HandleErrorsMongose;
