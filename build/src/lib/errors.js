"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class HandleError extends Error {
    constructor(name, message, status, from, messages) {
        super(message);
        this.name = name;
        this.message = message;
        this.messages = messages;
        this.status = status;
        this.from = from;
    }
}
exports.default = HandleError;
