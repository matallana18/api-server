"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const database_1 = __importDefault(require("./database"));
class Server {
    constructor(PORT) {
        console.log('initbd');
        this.port = PORT;
        this.app = express_1.default();
        this.database = new database_1.default();
    }
    static init_server(port) {
        return new Server(port);
    }
    start_server(callback) {
        this.app.listen(this.port, callback);
    }
}
exports.default = Server;
