"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const uri = 'mongodb://localhost/users';
class DataBaseConection {
    constructor() {
        DataBaseConection.init_bd();
        console.log('initbd');
    }
    static init_bd() {
        console.log('initbd');
        if (this.instance_mongoose) {
            return this.instance_mongoose;
        }
        else {
            this.instance_mongoose = mongoose.connect(uri, {
                useNewUrlParser: true,
                useCreateIndex: true
            })
                .then(db => {
                console.log('Conexión realizada.');
            })
                .catch(error => {
                console.error('Error conexión a base de datos.');
                console.error('Error:', error);
            });
        }
    }
}
exports.default = DataBaseConection;
