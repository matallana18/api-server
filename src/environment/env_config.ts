let PORT: Number = 0

PORT = Number(process.env.PORT) || 3000

export { PORT }