import { Document } from 'mongoose'

export default interface ModelUser extends Document {
    name: string;
    last_name: string;
    alias: string;
    user: string;
    password: string;
}