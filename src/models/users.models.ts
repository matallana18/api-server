/** Model Users */
import { Schema, model } from 'mongoose'
import ModelUser from './interfaces_models/model_users.interface'
import { transform_key } from '../controllers/aux_methods/aux_users.aux'
import { NextFunction } from 'express';
import error from '../lib/errors'

const user_schema: Schema<ModelUser> = new Schema(
    {
        name: { type: String, minlength: [3, 'ho is u name??... (min 3 characters)'] },
        last_name: { type: String, minlength: [3, 'ho is u last name??... (min 3 characters)'] },
        alias: { type: String, unique: [true, 'this alias is asignate of the other member.. try again pls!'], minlength: [2, 'it could be something like lazy bob (min 2 characters)'] },
        user: { type: String, required: [true, 'tell us your email pls.. friend!'] },
        password: { type: String, required: [true, 'just something u should know...'], minlength: [32, 'error.. password hash invalid'], select: false }
    }
)


user_schema.pre('validate', function (next: NextFunction) {
    try {

        const password = this.get('password')
        const new_password: string | boolean = transform_key(password)

        if (typeof new_password === 'boolean') {
            throw new error('Error in create user.', 'Errors validations', 403, 'Register Controllers', [{ message: 'Min length of characters is 8.. pls try again..', key: 'password' }])
        } else {
            this.set('password', new_password)
        }

        next()
    } catch (error) {
        next(error)
    }
})


export default model<ModelUser>('User', user_schema)