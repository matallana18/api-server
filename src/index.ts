/**
 * DESCRIPCIÓN DIRECTORIO SRC:
 * src /
 *      Controllers/:Todos los archivos que se encargaran de orquestar la aplicación
 *      lib/: Todos los archivos con codigo compartido entre la aplicación
 *      models/: Todos los archivos de modelos de la aplicación
 *      routes/: Todos los archivos de rutas de la aplicación
 *      views/: Todas las vistas de la aplicacíon
 *      server/: Archivos de servidor.
 *      index.ts : Punto de entrada de la aplicación
 * 
 * tips:
 *      npm install @types/express ayuda al tipado de typescripte para el framework express
 * 
 * Comandos configurados:
 *      npm run clean: (rm || del in windows -rf build) comando para eliminar la carpeta build que genera tsc 'typescriptcompiler'
 */

import express from 'express'
import morgan from 'morgan'
import cors from 'cors'
import { PORT } from './environment/env_config'
import index_routes from './routes/index_routes.routes'
import Server from './server/server'
import handleErrors from './middleware/handleErrors'

/* import routes from './routes/' */
const server = Server.init_server(PORT);
/** Settings server*/
server.app.use(morgan('dev'));

server.app.use(express.json());
/** entender datos de formulario */
server.app.use(express.urlencoded({ extended: false }))

server.app.use(cors({ origin: true }));

/** Middlewares */
/** Routes */
server.app.use('/api/inv-v1/', index_routes)

/** Static files */
server.app.use(handleErrors)

/** Start server */
server.start_server(() => {
    console.log('initbd');

    console.log('Server started on ports:', PORT)
})
