import express from 'express'
import auth_routes from './auth.routes'
import user_routes from './users.routes'
const app = express()

app.use('/auth', auth_routes)
app.use('/members', user_routes)
export default app