import { Router } from 'express';

import { AuthController } from '../controllers/auth.controllers'

const router: Router = Router();
const co_auth = new AuthController();

router.post('/login', co_auth.login);

export default router;
