import { Router } from 'express';

import { UserController } from '../controllers/users.controllers'
import auth from '../middleware/auth';

const router: Router = Router();
const co_us = new UserController();
router.get('/all_users', co_us.get_all_users)
router.post('/new', co_us.register_users);

export default router;
