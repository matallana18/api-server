import md5 from 'md5';
import { NextFunction } from 'express';
import Errors from '../../interfaces/errors.interface';
import error from '../../lib/errors'
import { HandleErrorsMongose } from '../../lib/handleErrorsMongoDB'


const transform_key = (password: string): string | boolean => {
    if (password.length >= 8) {
        return md5(password)
    } else {
        return false;
    }
}

const orquestrator_errors = (e: any, next: NextFunction) => {
    if (typeof e.code !== 'undefined' && e.code === 11000) {
        const mensaje: Errors[] = []
        mensaje.push({
            key: 'Alias',
            message: 'this alias is asignate of the other member.. try again pls!'
        })
        next(new error('Error in create user.', 'Errors validations', 403, 'Register Controllers', mensaje))
    }
    if (e.errors) {
        const mensaje = HandleErrorsMongose(e.errors)
        next(new error('Error in create user.', 'Errors validations', 403, 'Register Controllers', mensaje))
    }
    next(e)
}

export {
    transform_key,
    orquestrator_errors
}