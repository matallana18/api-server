
import { Request, Response, NextFunction } from 'express'
import Busers from '../interfaces/users.interface'
import usersModels from '../models/users.models'
import { orquestrator_errors } from './aux_methods/aux_users.aux'
import error from '../lib/errors'
export class UserController {

    get_all_users = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const users = await usersModels.find().lean().exec()
            if (!users) {
                throw new error('Error in search users', 'Errors searchs', 403, 'Search Controllers', [{ key: 'search', message: 'not found any user' }])
            }
            res.status(200).json({
                name: `Get all users`,
                message: `TEAM`,
                data: users,
                from: `Users controlers`
            })
        } catch (error) {
            next(error)
        }
    }

    register_users = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const user: Busers = req.body as Busers

            const save_user = await usersModels.create(user)

            res.status(200).json({
                name: `User created by ${save_user.alias || save_user.name}`,
                message: `Congrats ${save_user.alias || save_user.name}, you are ready in this site!`,
                data: save_user,
                from: `Users controlers`
            })

        } catch (e) {
            /** generar logica de mensajes, para n errores y un error */
            orquestrator_errors(e, next);
        }
    }
}
