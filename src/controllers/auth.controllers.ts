import { Request, Response, NextFunction } from 'express';
import usersModels from '../models/users.models'
import jwt from 'jsonwebtoken'
import Blogin from '../interfaces/login.interface'
import Busers from '../interfaces/users.interface'
import error from '../lib/errors'
import md5 from 'md5';
import { SECRET_KEY } from '../server/keys/key'
import { Iauth } from '../interfaces/auth.interface';
/* const app = express(); */

export class AuthController {

    login = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const credentials: Blogin = req.body as Blogin;
            console.log({ credentials });

            const user_email_valid = await usersModels.findOne({ user: credentials.user }).lean().exec() as Busers

            if (!user_email_valid) {
                throw new error('Auth Error', 'User not found', 404, 'Auth Controllers', [{ key: 'user or password', message: 'user or password wrong... try again pls' }])
            }

            const user_password_valid = await usersModels.findOne({ password: md5(credentials.password) }).lean().exec() as Busers

            if (!user_password_valid) {
                throw new error('Auth Error', 'User not found', 404, 'Auth Controllers', [{ key: 'user or password', message: 'user or password wrong... try again pls' }])
            }
            const token: string = jwt.sign(user_password_valid, SECRET_KEY, { expiresIn: '2h' })
            const login: Iauth = { expirte_in: '2h', token, user: user_password_valid }
            res.json(login)

        } catch (error) {
            next(error);
        }
    }

}