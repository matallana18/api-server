import { NextFunction, Request, Response } from 'express'
import { SECRET_KEY } from '../server/keys/key'
import jwt from 'jsonwebtoken'

function auth(request: Request, response: Response, next: NextFunction) {

    let token = request.get('token') as string

    if (token !== null) {
        jwt.verify(token, SECRET_KEY, (err, decoded) => {
            if (err) {
                switch (err.name) {
                    case 'TokenExpiredError':
                        response.status(401).json({
                            name: 'Token expired',
                            message: 'Token expired in server',
                            from: 'Validation token'
                        });
                        break;
                    case 'JsonWebTokenError':
                        response.status(401).json({
                            name: 'Token wrong',
                            message: 'Token wrong in server',
                            from: 'Validation token'
                        });
                        break;
                    default:
                        response.status(401).json({
                            name: 'Token wrong',
                            message: 'Token wrong in server',
                            from: 'Validation token'
                        });
                        break;
                }
            } else {
                next();
            }
        });
    } else {
        response.status(401).json({
            name: 'Token wrong',
            message: 'Token wrong in server',
            from: 'Validation token'
        });
    }
}

export default auth