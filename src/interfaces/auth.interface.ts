import Busers from "./users.interface";

export interface Iauth {
    token: string
    user: Busers
    expirte_in: '2h'
}