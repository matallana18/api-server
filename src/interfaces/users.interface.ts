export default interface Busers {
    name: string;
    last_name: string;
    alias: string;
    user: string;
    password: string;
}