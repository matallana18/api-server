export default interface Errors {
    message: string[] | string;
    key: string;
}