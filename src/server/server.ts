
import express from 'express'
import database from './database';

export default class Server {
    app: express.Application;
    port: Number;
    database: any;

    constructor(PORT: Number) {
        this.port = PORT;
        this.app = express();
        this.database = new database();
    }

    static init_server(port: Number): Server {
        return new Server(port);
    }


    start_server(callback: any): void {
        this.app.listen(this.port, callback);
    }

}