import Errors from "../interfaces/errors.interface"

export function HandleErrorsMongose(errores: any): Errors[] {
    const mensaje: Errors[] = []
    for (let key in errores) {
        mensaje.push({ message: errores[key].message, key });
    }
    return mensaje;
}